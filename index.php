<!DOCTYPE html>
<html>
  <?php include('inclure/settings.php'); ?>
  <?php include('inclure/head.php'); ?>
  <body>
    <header>
      <h1><?= $site->title ?></h1>
      <h2><?= $site->description ?></h2>
      <ul>
        <?php foreach ($social as $source => $caption): ?>
          <li><a href="<?= $source ?>"><?= $caption ?></a></li>
        <?php endforeach; ?>
      </ul>
    </header>
    <?php foreach ($showcase as $source => $caption): ?><a href="showcase/<?= $source ?>"><figure>
      <?php if(isVideo($source)): ?>
        <video autoplay muted loop>
          <source src="showcase/<?= $source ?>" type="video/mp4">
        </video>
      <?php else: ?>
        <img src="showcase/<?= $source ?>">
      <?php endif; ?>
      <figcaption><?= $caption ?></figcaption>
    </figure></a><?php endforeach; ?>
  </body>
</html>
