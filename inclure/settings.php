<?php
  function isVideo($filename) {
    if (endsWith($filename, '.mp4')) {
      return true;
    } else {
      return false;
    }
  }
  
  function endsWith($haystack, $needle) {
      $length = strlen( $needle );
      if( !$length ) {
          return true;
      }
      return substr( $haystack, -$length ) === $needle;
  }
  
  $settings_file = file_get_contents('settings.json');
  $settings = json_decode($settings_file);
  $site = $settings->site;
  $social = $settings->social;
  $showcase = $settings->showcase;
?>
