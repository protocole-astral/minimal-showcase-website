<head>
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta charset="utf-8">

  <title><?= $site->title ?></title>
  <meta name="description" content="<?= $site->description ?>">
  <meta name="keywords" content="<?= $site->keywords ?>">
  <meta name="news_keywords" content="<?= $site->keywords ?>">
  <meta name="robots" content="index,follow" />

  <meta property="og:title" content="<?= $site->title ?>" />
  <meta property="og:site_name" content="<?= $site->description ?>" />
  <meta property="og:description" content="<?= $site->description ?>" />
  <meta property="og:image" content="<?= $site->image ?>" />
  <meta property="og:locale" content="<?= $site->lang ?>" />
  <meta property="twitter:title" content="<?= $site->title ?>" />
  <meta property="twitter:description" content="<?= $site->description ?>" />
  <meta property="twitter:image" content="<?= $site->image ?>" />
  <meta property="twitter:card" content="summary_large_image" />
  <meta property="fb:title" content="<?= $site->title ?>" />
  <meta property="fb:description" content="<?= $site->description ?>" />
  <meta property="fb:image" content="<?= $site->image ?>" />
  <meta property="ownpage:title" content="<?= $site->title ?>" />
  <meta property="ownpage:description" content="<?= $site->description ?>" />
  <meta property="ownpage:image" content="<?= $site->image ?>" />

  <link rel="icon" type="image/x-icon" href="<?= $site->icon ?>" />
  <link rel="shortcut icon" type="image/x-icon" href="<?= $site->icon ?>" />

  <link rel="stylesheet" type="text/css" href="assets/css/style.css"/>
</head>
